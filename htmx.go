package mycal

import (
	"fmt"
	"html/template"
	"net/http"

	"github.com/gorilla/mux"
)

var tmpl *template.Template

//TODO: use code gen to create a static file
func index(w http.ResponseWriter, r *http.Request) {
	err := tmpl.ExecuteTemplate(w, "index.html", struct{}{})
	if err != nil {
		fmt.Println("Could not execute template", err)
	}
}

func calendar(w http.ResponseWriter, r *http.Request) {
	//TODO: Pull the date out of the URL or default to today
	c := createCalendar(2024, 1)
	err := tmpl.ExecuteTemplate(w, "show-calendar.html", c)
	if err != nil {
		fmt.Println("Could not execute template", err)
	}
}

//could change to serve a static file
func api(w http.ResponseWriter, r *http.Request) {
	details := getResources()
	err := tmpl.ExecuteTemplate(w, "api.html", details)
	if err != nil {
		fmt.Println("Could not execute template", err)
	}
}

func notFound(w http.ResponseWriter, r *http.Request) {
	s := fmt.Sprintf("<h2>404 Could not find!</h2><p>Path Provided: %s</p>", r.URL)
	fmt.Fprintf(w, s)
}

func addHTMLRoutes(r *mux.Router) {
	tmpl = template.Must(template.ParseGlob("templates/*.html"))
	r.HandleFunc("/", index).Methods("GET")
	r.HandleFunc("/api", api).Methods("GET")
	r.HandleFunc("/calendar", calendar).Methods("GET")

	s := http.StripPrefix("/static/", http.FileServer(http.Dir("./static/")))
	r.PathPrefix("/static/").Handler(s)

	r.NotFoundHandler = http.HandlerFunc(notFound)
}
