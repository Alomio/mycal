package mycal

import (
	"time"
)

//CalendarDay a day on a calendar
type CalendarDay struct {
	Weekday uint8
	Day     uint8
}

func createCalendar(y, m int) []CalendarDay {
	//we can use time.January to add clarity to the month
	start := time.Date(y, time.Month(m), 1, 0, 0, 0, 0, time.UTC)
	end := time.Date(y, time.Month(m+1), 0, 0, 0, 0, 0, time.UTC)

	month := make([]CalendarDay, end.Day())
	end = end.AddDate(0, 0, 1)
	for start.Before(end) {
		month[start.Day()-1].Weekday = weekdayValue(start.Weekday()) + 1
		month[start.Day()-1].Day = uint8(start.Day())
		start = start.AddDate(0, 0, 1)
	}

	return month
}

func weekdayValue(d time.Weekday) uint8 {
	switch d {
	case time.Sunday:
		return 0
	case time.Monday:
		return 1
	case time.Tuesday:
		return 2
	case time.Wednesday:
		return 3
	case time.Thursday:
		return 4
	case time.Friday:
		return 5
	case time.Saturday:
		return 6
	default:
		//TODO: probably don't want to panic
		panic("unknown day of the week!")
	}
}
