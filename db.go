package mycal

import (
	"database/sql"
	"fmt"
	//load all of the driver info required to work with SQLite3
	_ "github.com/mattn/go-sqlite3"
)

var DB *sql.DB

func connectToSQLiteDB() error {
	db, err := sql.Open("sqlite3", "./mycal.db")
	if err != nil {
		return err
	}

	// Ping the database to ensure connectivity
	err = db.Ping()
	if err != nil {
		return err
	}

	fmt.Println("Connected to SQLite Database!")
	DB = db
	return nil
}
