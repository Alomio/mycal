package main

import (
	"fmt"
	"net/http"

	"gitlab.com/Alomio/mycal"
)

func main() {
	//err := mycal.Connect()
	//if err != nil {
	//	log.Fatal("Unable to create a connection to the database", err)
	//}
	//defer mycal.DB.Close()

	//Create a logger
	//mycal.SetupLogger()

	//Set up mux router
	mux := mycal.NewServer()

	//Runnit
	fmt.Println("Starting on http://0.0.0.0:8080/")
	http.ListenAndServe(":8080", mux)
}
