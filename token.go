package mycal

import (
    "encoding/base64"
	"crypto/rand"
)

func generateToken() (string, error) {
    b := make([]byte, 32)
    _, err := rand.Read(b)
    if err != nil {
        return "", err
    }
    v := base64.StdEncoding.EncodeToString(b)
    return v, nil
}
