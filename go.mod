module gitlab.com/Alomio/mycal

go 1.21.1

require (
	github.com/gorilla/mux v1.8.1
	github.com/mattn/go-sqlite3 v1.14.18
)
