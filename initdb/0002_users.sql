-- Table to store users so they can login
CREATE TABLE users (
    -- unique id
    id      CHAR(21) not null primary key DEFAULT 'u' || xid(),
    -- some name for the user
    name    VARCHAR(48) not null,
    -- salt for this user's password
    salt    CHAR(8)     not null,
    -- hashed password
    passwd  CHAR(64)    not null
);

-- Example user with classic admin/admin
INSERT INTO users(name, salt, passwd) VALUES('admin', 'CWWo5vK7', '1bfc36ea1c53e0c49adfea4b4799d47b3d2e2e2b010e603340aa218842bba776');
