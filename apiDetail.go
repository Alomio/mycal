package mycal

//This file really needs to get auto generated from something else.
//no one, especially me, wants to write down every data and it's prop for each struct.
//I'm thinking that we can read in a CUE file and use reflection or something
//or maybe we just define the OG structs as they are and this gets created.

type APIDocs struct {
	Resources APIResources
}

type APIResources struct {
	Public  []AppResource
	Private []AppResource
}

type AppResource struct {
	Name  string
	Props []ResourceProp
}

type ResourceProp struct {
	Name string
	Type string
}

func getResources() APIDocs {
	User := AppResource{
		Name: "User",
		Props: []ResourceProp{
			{Name: "username", Type: "string"},
			{Name: "age", Type: "u8"},
			{Name: "hungry", Type: "bool"},
		},
	}

	Task := AppResource{
		Name: "Task",
		Props: []ResourceProp{
			{Name: "name", Type: "string"},
			{Name: "summary", Type: "string"},
			{Name: "date", Type: "datetime"},
			{Name: "completed", Type: "bool"},
		},
	}
	r := APIResources{
		Public:  []AppResource{Task},
		Private: []AppResource{User},
	}
	a := APIDocs{r}
	return a
}
