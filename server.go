package mycal

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
)

//TODO: Set format for errors to be consistent

type Thing struct {
	Age   uint8  `json:"age"`
	Color string `json:"color"`
}

func getThings(w http.ResponseWriter, _ *http.Request) {
	t := Thing{
		78,
		"Green",
	}

	b, err := json.Marshal(t)
	if err != nil {
		fmt.Println("could not marshal thing into a JSON", err)
	}

	w.Write(b)
}

func getAThing(w http.ResponseWriter, r *http.Request) {
	id, ok := mux.Vars(r)["id"]
	if !ok {
		fmt.Println("Could not find ID in the path variables")
	}

	fmt.Fprintf(w, `{"id": "%s", "age": "12", "color": "red"}`, id)
}

// There is some discorse on the the proper way to unmarshal a JSON
// when using net/http or gorilla/mux. Basically, to stream or not to stream.
// https://stackoverflow.com/questions/15672556/handling-json-post-request-in-go#answer-55052845
// I like this way because the stream will throw an error if an unexpected field is sent.
// I find it much better to be strict on your payloads than flexible. New users may be annoyed
// but your service is much more likely to work in the longterm
func makeAThing(w http.ResponseWriter, r *http.Request) {
	d := json.NewDecoder(r.Body)
	d.DisallowUnknownFields() // catch unwanted fields
	var t Thing
	err := d.Decode(&t)
	if err != nil {
		// bad JSON or unrecognized json field
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	fmt.Println("Recieved request to make thing!", t)
	fmt.Fprintln(w, `{"msg": "success"}`)
}

func generateAPIToken(w http.ResponseWriter, r *http.Request) {
	t, err := generateToken()
	if err != nil {
		fmt.Println("unable to create API Token", err)
		http.Error(w, "Unable to make Token", http.StatusInternalServerError)
		return
	}
    fmt.Fprintf(w, `{"token": "%s"}`, t)
}

func addRESTRoutes(r *mux.Router) {
    s := r.PathPrefix("/v1").Subrouter()

	s.HandleFunc("/token", generateAPIToken).Methods("GET")
	s.HandleFunc("/things", getThings).Methods("GET")
	s.HandleFunc("/things/{id}", getAThing).Methods("GET")
	s.HandleFunc("/things", makeAThing).Methods("POST")
}

func NewServer() *mux.Router {
	r := mux.NewRouter()
	addHTMLRoutes(r)
	addRESTRoutes(r.PathPrefix("/api").Subrouter())

	return r
}
